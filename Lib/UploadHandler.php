<?php

namespace Nitra\UploadBundle\Lib;

use Symfony\Component\DependencyInjection\Container;

/**
 * Класс обработки загруженных файлов
 */
class UploadHandler
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    
    protected $name;
    protected $server_name;
    protected $upload_dir;
    protected $useOriginalName = false; //Использовать оригинальное имя или генерировать 

    /**
     * Принимает информацию о загруженном файла
     */
    public function __construct(Container $container)
    {
        $this->container        = $container;
        $this->upload_dir       = 'images';
    }

    /**
     * Возвращает абсолютный путь к папке загрузки
     * @return type
     */
    public function getAbsolutePath()
    {
        return $this->getUploadRootDir() . '/' . $this->getUploadDir();
    }

    /**
     * Абсолютный путь к папке загрузки файлов
     * @return type
     */
    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return "{$this->container->get('kernel')->getRootDir()}/../web/{$this->getUploadDir()}";
    }

    /**
     * Путь загрузки файла относительно каталога app
     * @return string
     */
    protected function getUploadDir()
    {
        return $this->upload_dir;
    }

    /**
     * Путь загрузки файла относительно каталога app
     */
    public function setUploadDir($dir)
    {
        $this->upload_dir = $dir;
    }

    /**
     * Путь загрузки файла относительно каталога app
     */
    public function setUseOriginalName($useOriginalName)
    {
        $this->useOriginalName = $useOriginalName;
    }

    /**
     * Возвращает путь относительно / к загруженному файлу с его именем
     * @return string
     */
    public function getFilePath($file)
    {
        return $this->getUploadRootDir() . '/' . $file->getServerName();
    }

    /**
     * Сохраняет загруженный файл в папку
     * @return \Nitra\UploadBundle\Classes\UploadHandler
     */
    public function upload(\Symfony\Component\HttpFoundation\File\UploadedFile $file)
    {
        if ($file === null) {
            return;
        }
        $this->name = $file->getClientOriginalName();
        preg_match('/(.*)\.(.*)/', $file->getClientOriginalName(), $out);
        $name   = key_exists(1, $out) ? $out[1] : uniqid();
        $ext    = key_exists(2, $out) ? $out[2] : null;
        
        if ($this->useOriginalName) {
            $this->server_name = $this->uniqFriendlyURL($name, $ext);
        } else {
            $this->server_name = uniqid() . '.' . $ext;
        }
        $file->move($this->getUploadRootDir(), $this->server_name);
        
        return $this;
    }

    /**
     * Возвращает информацию о загруженном файле в формате json
     * @return json
     */
    public function getFileInfo()
    {
        $return = array();
        $return['files'] = array(array(
            'id'            => $this->server_name,
            'name'          => $this->name,
            'path'          => '/' . $this->getUploadDir() . '/' . $this->server_name,
            'delete_type'   => 'POST',
        ));
        
        return json_encode($return);
    }

    /**
     * Удаляет файл
     */
    public function remove($file)
    {
        $file = str_replace('-', '/', $file);
        @unlink("{$this->container->get('kernel')->getRootDir()}/../web{$file}");
    }

    /**
     * Сгенирировать уникальное имя файла
     * @param string $name  - название файла
     * @param string $ext   - расширение файла
     * @return string Название файла + расширения
     */
    protected function uniqFriendlyURL($name, $ext)
    {
        //Транслит для имени файла
        $fileName = $this->friendlyURL($name);
        return $this->uniqueFileName($fileName, $ext) . '.' . $ext;
    }
    
    /**
     * Поиск файла и доклеивание '_x' в случае существования
     * @param string $fileName
     * @return string
     */
    protected function uniqueFileName($fileName, $ext)
    {

        $newName = $fileName;           
        for($i = 1; file_exists($this->getUploadRootDir() . '/' .  $newName . '.' . $ext); $i++)
        {
            $newName = $fileName. "_$i";
        }

        return $newName;
    }

    /**
     * Преобразование URL
     */
    protected function friendlyURL($string)
    {
        $lit = array(
            "а" => "a",
            "б" => "b",
            "в" => "v",
            "г" => "g",
            "д" => "d",
            "е" => "e",
            "ё" => "yo",
            "ж" => "zh",
            "з" => "z",
            "и" => "i",
            "й" => "y",
            "к" => "k",
            "л" => "l",
            "м" => "m",
            "н" => "n",
            "о" => "o",
            "п" => "p",
            "р" => "r",
            "с" => "s",
            "т" => "t",
            "у" => "u",
            "ф" => "f",
            "х" => "h",
            "ц" => "ts",
            "ч" => "ch",
            "ш" => "sh",
            "щ" => "shch",
            "ъ" => "",
            "ы" => "i",
            "ь" => "",
            "э" => "e",
            "ю" => "yu",
            "я" => "ya",
            "А" => "A",
            "Б" => "B",
            "В" => "V",
            "Г" => "G",
            "Д" => "D",
            "Е" => "E",
            "Ё" => "Yo",
            "Ж" => "Zh",
            "З" => "Z",
            "И" => "I",
            "Й" => "Y",
            "К" => "K",
            "Л" => "L",
            "М" => "M",
            "Н" => "N",
            "О" => "O",
            "П" => "P",
            "Р" => "R",
            "С" => "S",
            "Т" => "T",
            "У" => "U",
            "Ф" => "F",
            "Х" => "H",
            "Ц" => "Ts",
            "Ч" => "Ch",
            "Ш" => "Sh",
            "Щ" => "Shch",
            "Ъ" => "",
            "Ы" => "I",
            "Ь" => "",
            "Э" => "E",
            "Ю" => "Yu",
            "Я" => "Ya",
            "є" => "e",
            "Є" => "E",
            "і" => "i",
            "І" => "I",
            "ї" => "yi",
            "Ї" => "Yi",
        );
        $string = strtr($string, $lit);
        $string = preg_replace("`\[.*\]`U", "", $string);
        $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i', '_', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace("`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i", "\\1", $string);
        $string = preg_replace(array("`[^a-z0-9]`i", "`[-]+`"), "_", $string);

        return strtolower(trim($string, '-'));
    }
}