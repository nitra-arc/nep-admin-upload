<?php

namespace Nitra\UploadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadController extends Controller
{
    /** @return \Nitra\UploadBundle\UploadHandler */
    protected function getUploadHandler() { return $this->get('upload_handler'); }
    
    /**
     * Обрабатывает загруженные файлы и сохраняет. Удаляет файл.
     *
     * @param  int|null                                   $id файла
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function attachAction(Request $request, $id)
    {
        $uploadH = $this->getUploadHandler();

        if (!$id) {// если не передан id - загружаем новый файл
            // поиск поля, которое содержит загруженные файлы
            $file = $request->files->get('nlupload');
            // если пришло что то непонятное - возвращаем ошибку
            if (!is_object($file) && !($file instanceof UploadedFile)) {
                $min = $this->inMBytes(ini_get('post_max_size')) < $this->inMBytes(ini_get('upload_max_filesize'))
                    ? ini_get('post_max_size')
                    : ini_get('upload_max_filesize');
                return $this->sendError('errors.large', array(
                    '%max%'   => " (" . $this->inMBytes($min) . " Mb)",
                ));
            }
            
            if ($this->container->hasParameter('nlupload_max_file_size')) {
                $mxf = $this->container->getParameter('nlupload_max_file_size');
                if (is_array($mxf)) {
                    $key = key_exists(strtolower($file->getClientOriginalExtension()), $mxf) ? strtolower($file->getClientOriginalExtension()) : 'default';
                    if (($file->getSize() / 1024 / 1024) > $mxf[$key]) {
                        return $this->sendError('errors.large', array(
                            '%max%'   => " ($mxf Mb)",
                        ));
                    }
                } elseif (!is_array($mxf) && ($file->getSize() / 1024 / 1024) > $mxf) {
                    return $this->sendError('errors.large', array(
                        '%max%'   => " ($mxf Mb)",
                    ));
                }
            }

            // устанавливаем путь для загрузки
            $uploadH->setUploadDir($request->request->get('base_path'));
            // устанавливаем параметр использования оригинального имени
            $uploadH->setUseOriginalName($request->request->get('use_original_name') === 'true');
            $uploadH->upload($file);

            // файл успешно загружен, возвращаем информацию о нем
            return new Response($uploadH->getFileInfo());
        } else { // если есть id - удаляем файл
            $uploadH->remove($id);
            return new Response();
        }
    }
    
    protected function sendError($error, array $parameters = array())
    {
        $msg = $this->get('translator')->trans($error, $parameters, 'NitraUploadBundle');
        return new Response($msg);
    }

    /**
     * @param string $val
     * @return int
     */
    protected function inMBytes($val)
    {
        $val  = trim($val);
        $last = strtolower($val[strlen($val)-1]);
        switch($last) {
            case 'g':
                $val /= 1024;
            case 'm':
                $val /= 1024;
            case 'k':
                $val /= 1024;
        }

        return $val * 1024 * 1024;
    }
}