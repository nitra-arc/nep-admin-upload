# UploadBundle

## Описание

данный бандл предназначен для:

* загрузки файлов

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-uploadbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\UploadBundle\NitraUploadBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

## Настройка


* **parameters.yml**

 * **nlupload_max_file_size** - максимально допустимый размер загружаемых файлов (указывается в **мегабайтах**), если не указывать - размер файла проверяться не будет

```yml
parameters:
    #...
    nlupload_max_file_size: 50
    #...
```
или

```yml
parameters:
    #...
    nlupload_max_file_size:
      fileType: size
      png: 10
    #...
```

## Добавление формы:

* **php**

* **required** - **true** - обязательное поле **false** - не обязательное поле
* **base_path** - куда загружать (по умолчанию web/images)
* **use_original_name** - использовать оригинальное название картинки
* **disabled**  отключть активность, 
* **fileTypes**  массив доступных типов файлов для загрузки, 

* **preCallback** js при добавлении изображения,
* **postCallback** js после добавлении изображения, 
* **postRemove**  js после удаления изображения, 
* **maxLengthFileUpload** максимальное допустимое количество изображений для загрузки (только для multiple)

```php
        $builder->add('fildname', 'nlupload', array(
            'required'          => true,
            'base_path'         => null,
            'use_original_name' => false, 
            'preCallback'         => '',
            'postCallback'        => '',
            'postRemove'          => '',
            'maxLengthFileUpload' => null,
            'disabled'            => false,
            'fileTypes'           => array('jpg','png'),
        ));
```