(function( $ ) {
    $.fn.nlupload = function(options) {
        var settings = $.extend({
            id:                 '',// '{{ form.vars.id }}',
            name:               '',// '{{ form.vars.name }}',
            description:        '',// '{{ form.vars.description }}',
            full_name:          '',// '{{ form.vars.full_name }}',
            tableId:            '',// '{{ form.vars.full_name }}',
            base_path:          '',// '{{ base_path }}',
            use_original_name:  false,//{{ use_original_name ? 'true' : 'false' }}
            multiple:           false,//{{ multiple }}
            msg_delete_old:     '',// {{ 'deleteOld'|trans }}
            msg_delete_confirm: '',// {{ 'deleteConfirm'|trans }}
            msg_loading:        '',// {{ 'loading'|trans }},
            path:               '',// {{ path(attach_route) }}
            error_413:          '',// {{ 'errors.large'|trans({'%max%': ''}, 'NitraUploadBundle') }}
            error_undefined:    '',// {{ 'errors.fail'|trans }}
            maxLengthFileUpload:  null,
            ajax:               false,
            preCallback:        null,
            postCallback:       null,
            postRemove:         null,
            disabled:           false,
            fileTypes:          ''
        }, options); 

        var filesTable = '#files_' + settings.full_name.replace(/\[/g, '_').replace(/\]/g, '_') + (settings.ajax ? '_colorbox' : '');
        return this.each(function() {
            $(this).fileupload({
                autoUpload:         true,
                dropZone:           filesTable,
                filesContainer:     filesTable,
                uploadTemplateId:   null,
                downloadTemplateId: null,
                acceptFileTypes:    settings.fileTypes,
                formData: function(form) {
                    var data = form.find((settings.ajax ? '#cboxLoadedContent ' : '') + 'div.form_row.field_' + settings.name).serializeArray();
                    data.push({
                        name:           'base_path',
                        value:          settings.base_path,
                        description:    settings.description
                    });
                    data.push({
                        name:           'use_original_name',
                        value:          settings.use_original_name,
                        description:    settings.description
                    });
                    return data;
                },
                add: function(e, data) {
                    if ((!settings.multiple) && ($(filesTable + ' button').length) > 0) {
                        alert(settings.msg_delete_old);
                        return false;
                    }
                    if(settings.multiple) {
                        // проверка на допустимый лимит количества файлов для загрузки
                        var countRow = $('#'+settings.tableId+' tr').length;
                        if(settings.maxLengthFileUpload && settings.maxLengthFileUpload <= countRow) {
                            alert(settings.msg_limit_upload);
                            return false;
                        }
                    }
                    settings.preCallback && settings.preCallback();
                    data.submit();
                },
                destroy: function(e, data) {
                    var del_confirm = confirm(settings.msg_delete_confirm);
                    if (!del_confirm) {
                        return;
                    }
                    var that = $(this).data('fileupload');
                    if (data.url) {
                        $.ajax(data);
                        that._adjustMaxNumberOfFiles(1);
                    }
                    that._transition(data.context).done(function() {
                        $(this).remove();
                        that._trigger('destroyed', e, data);
                    });
                    $('#file_' + settings.id).attr('name', $(filesTable).find('tr').length ? '' : (settings.full_name));
                    // возможность добавления доп действий после удаления блока с изображением
                    options.postRemove && options.postRemove();
                },
                uploadTemplate: function(o) {
                    var rows = $();
                    $.each(o.files, function(index, file) {
                        var row = $('<tr class="template-upload fade">' +
                            '<td class="preview"></td><td class="name error"></td><td class="cancel"><button type="button" class="delete link_icon_mini" ' + (settings.disabled ? 'disabled ' : '') + '><i class="i_delete"></i></button></td></tr>');
                        row.find('.name').text(file.name + ' - ' + settings.msg_loading);
                        if (file.error) {
                            row.find('.error').text(file.error);
                        }
                        rows = rows.add(row);
                    });
                    return rows;
                },
                downloadTemplate: function(o) {
                    var rows = $();
                    var iteration = 0;
                    $(filesTable+' tbody tr').each(function(){
                       iteration =  ( $(this).attr('iteration') && $(this).attr('iteration') >= iteration )
                                    ? $(this).attr('iteration') *1+1
                                    : iteration;
                    });
                    $.each(o.files, function(index, file) {
                        
                        var imagePath = settings.multiple ? '[]' : '';
                            imagePath = settings.description ? '['+iteration+'][path]' : imagePath;
                        
                        var row = $('<tr class="template-download fade" iteration="'+ iteration +'">' +
                            '<td class="preview"></td>' +
                            (file.error
                                ? '<td class="name error"></td>'
                                : '<td class="name"><a></a></td>') +
                            (settings.description
                                ? '<td class="description"><input type="text" name="' + settings.full_name + ( (settings.multiple || settings.description) ? '['+iteration+'][description]' : '') + '"' + (settings.disabled ? ' disabled ' : '') + ' /></td>'
                                : '') +
                            '<td class="delete"><button class="delete link_icon_mini"' + (settings.disabled ? ' disabled' : '') + '><i class="i_delete"></i></button> ' +
                            '<!--<input type="checkbox" name="delete" value="1">-->' +
                            '<input type="hidden" name="' + settings.full_name + imagePath + '" value="' + file.path + '">' + '</td></tr>');
                        if (file.error) {
                            row.find('.name').text(file.name);
                            row.find('.error').text(file.error);
                        } else {
                            row.find('.name a').text(file.name);
                            if (file.path) {
                                var fileExt = file.path.split('.');
                                var extArr = ["jpg", "jpeg", "png", "gif", "ico", "tif"];
                                if ($.inArray(fileExt[fileExt.length-1], extArr) !== -1) {
                                    row.find('.preview').addClass(fileExt[fileExt.length-1]);
                                } else {
                                    row.find('.preview').addClass('other_file_ext ' + fileExt[fileExt.length-1]);
                                }
                                if ($.inArray(fileExt[fileExt.length-1], extArr) !== -1) {
                                    row.find('.preview').append('<a><img></a>')
                                        .find('img')
                                            .prop('src', file.path)
                                            .css({
                                                'max-height': '50px',
                                                'max-width': '50px'
                                            });
                                } else {
                                    row.find('.preview').append('<a href=' + file.path + ' target="_blank">' + file.name + '</a>')
                                }
                                row.find('a').prop('rel', 'gallery');
                            }
                            row.find('a').prop('href', file.url);
                            row.find('.delete button')
                                .attr('data-type', file.delete_type)
                                .attr('data-url', settings.path + "/" + file.path.replace(/\//gi, '-')); // hack
                        }
                        rows = rows.add(row);
                    });
                    $('#file_' + settings.id).attr('name', (o.files.length + $(filesTable).find('tr').length) ? '' : settings.full_name);
                    return rows;
                },
                fail: function(e, data) {
                    if (data.jqXHR === undefined) {
                        var aceptedFileTypesErr = settings.fileTypes.toString().replace(/\/\\.\(/, '').replace(/\)\$\/i/,'').replace(/\|/g,', ');
                        alert('Файл должен иметь один из форматов ' + aceptedFileTypesErr);
                    } else if (data.jqXHR.status == 413) {
                        alert(settings.error_413);
                    } else if (data.jqXHR.status == 200) {
                        alert(data.jqXHR.responseText);
                    } else {
                        alert(settings.error_undefined);
                    }
                },
                done: function (e, data) {
                    var that = $(this).data('fileupload'),
                        files = that._getFilesFromResponse(data),
                        template,
                        deferred;
                    if (data.context) {
                        data.context.each(function(index) {
                            var file = files[index] ||
                                    {error: 'Empty file upload result'},
                            deferred = that._addFinishedDeferreds();
                            if (file.error) {
                                that._adjustMaxNumberOfFiles(1);
                            }
                            that._transition($(this)).done(
                                    function() {
                                        var node = $(this);
                                        template = that._renderDownload([file])
                                                .replaceAll(node);
                                        that._forceReflow(template);
                                        that._transition(template).done(
                                                function() {
                                                    data.context = $(this);
                                                    that._trigger('completed', e, data);
                                                    that._trigger('finished', e, data);
                                                    deferred.resolve();
                                                }
                                        );
                                    }
                            );
                        });
                    } else {
                        if (files.length) {
                            $.each(files, function(index, file) {
                                if (data.maxNumberOfFilesAdjusted && file.error) {
                                    that._adjustMaxNumberOfFiles(1);
                                } else if (!data.maxNumberOfFilesAdjusted &&
                                        !file.error) {
                                    that._adjustMaxNumberOfFiles(-1);
                                }
                            });
                            data.maxNumberOfFilesAdjusted = true;
                        }
                        template = that._renderDownload(files)
                                .appendTo(that.options.filesContainer);
                        that._forceReflow(template);
                        deferred = that._addFinishedDeferreds();
                        that._transition(template).done(
                            function() {
                                data.context = $(this);
                                that._trigger('completed', e, data);
                                that._trigger('finished', e, data);
                                deferred.resolve();
                            }
                        );
                    }
                    settings.postCallback && settings.postCallback();
                }
            });
        });
    };
})(jQuery);