<?php

namespace Nitra\UploadBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UploadType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['base_path']            = $options['base_path'];
        $view->vars['multiple']            = $options['multiple'];
        $view->vars['use_original_name']   = $options['use_original_name'];
        $view->vars['write_file_name']     = $options['write_file_name'];
        $view->vars['attach_route']        = $options['attach_route'];
        $view->vars['description']         = $options['description'];
        $view->vars['preCallback']         = $options['preCallback'];
        $view->vars['postCallback']        = $options['postCallback'];
        $view->vars['postRemove']          = $options['postRemove'];
        $view->vars['maxLengthFileUpload'] = $options['maxLengthFileUpload'];
        $view->vars['disabled']            = $options['disabled'];
        $view->vars['fileTypes']           = $options['fileTypes'];
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'multiple'            => false,
            'error_bubbling'      => false,
            'base_path'           => null,
            'use_original_name'   => false,
            'write_file_name'     => false,
            'description'         => false,
            'attach_route'        => 'nl_upload_attach_file',
            'preCallback'         => '',
            'postCallback'        => '',
            'postRemove'          => '',
            'maxLengthFileUpload' => null,
            'disabled'            => false,
            'fileTypes'           => array(),
        ));
    }

    public function getName()
    {
        return 'nlupload';
    }
    
    public function getParent()
    {
        return 'hidden';
    }
}