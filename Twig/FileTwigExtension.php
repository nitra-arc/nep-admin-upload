<?php

namespace Nitra\UploadBundle\Twig;

class FileTwigExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('ext', array($this, 'ext')),
        );
    }

    public function ext($filepath)
    {
        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        return $ext;
    }
    
    public function getName()
    {
        return 'file_extension';
    }

}
